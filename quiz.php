<?php

$a=array("A","B","C","A","B");
print_r(array_count_values($a));
echo "<hr>";

$a=array("a"=>"Volvo","b"=>"BMW","c"=>"Toyota");
print_r(array_reverse($a));
echo "<hr>";

$a=array("a"=>"red","b"=>"green","c"=>"blue");
echo array_search("red",$a);
echo "<hr>";

$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("a"=>"purple","b"=>"orange");
array_splice($a1,0,2,$a2);
print_r($a1);
echo "<hr>";

$a=array("a"=>"red","b"=>"green");
array_unshift($a,"blue");
print_r($a);
echo "<hr>";

$a=array("Name"=>"Peter","Age"=>"41","Country"=>"USA");
print_r(array_values($a));
echo "<hr>";

$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("e"=>"red","f"=>"green","g"=>"blue");
$result=array_diff($a1,$a2);
print_r($result);
echo "<hr>";

function test_odd($var)
{
    return($var & 1);
}
$a1=array("a","b",2,3,4);
print_r(array_filter($a1,"test_odd"));
echo "<hr>";

$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$result=array_flip($a1);
print_r($result);
echo "<hr>";

$a=array("Volvo"=>"XC90","BMW"=>"X5","Toyota"=>"Highlander");
print_r(array_keys($a));
echo "<hr>";

$a1=array("red","green");
$a2=array("blue","yellow");
print_r(array_merge($a1,$a2));
echo "<hr>";

$a=array("red","green");
array_push($a,"blue","yellow");
print_r($a);
echo "<hr>";

$a=array("red","green","blue","yellow","brown");
$random_keys=array_rand($a,3);
echo $a[$random_keys[0]]."<br>";
echo $a[$random_keys[1]]."<br>";
echo $a[$random_keys[2]];
echo "<hr>";

$a1=array("red","green");
$a2=array("blue","yellow");
print_r(array_replace($a1,$a2));
echo "<hr>";

var_dump((bool) "false");
echo "<hr>";

$value ='1234.10abc ';
echo intval($value);
echo "<hr>";

is_bool(1);
echo "<hr>";

$foo = "1abc"; $bar = true;
echo settype($foo, "integer");
echo "<br>";
echo settype($bar, "string");
echo "<hr>";

class StrValTest
{
    public function __toString()
    {
        return __CLASS__;
    }
}
echo strval(new StrValTest);

echo "<hr>";

$var1 = 'Hello World';
$var2 = '';
$var2 =& $var1;
debug_zval_dump($var1);
echo "<hr>";



?>